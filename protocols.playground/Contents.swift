protocol PlayerProtocol {
    var hitPoints : Int {get}
    var name : String {get}
    init(playerName:String , startingHealth:Int)
    mutating func takeDamage(damage:Int)
}

protocol MagicProtocol : PlayerProtocol {
    func fireAttack() -> Int
    func iceAttack() -> Int
    func waterAttack() -> Int
}

protocol BossProtocol {
    func SuperAttack() -> Int
    mutating func healHealth()
}

extension PlayerProtocol {
    func isAlive() -> Bool {
        return hitPoints > 0
    }
}

extension BossProtocol {
    func tauntPlayer(){
        print("I am gonna crush you !")
    }
}


struct Wizard : MagicProtocol {
    var hitPoints: Int
    var name: String
    init(playerName: String, startingHealth: Int) {
        self.name = playerName
        self.hitPoints = startingHealth
    }
    mutating func takeDamage(damage: Int) {
        hitPoints -= damage
    }
    func fireAttack() -> Int {
        return 10
    }
    func iceAttack() -> Int {
        return 20
    }
    func waterAttack() -> Int {
        return 30
    }
}

struct MiniBoss : PlayerProtocol , BossProtocol {
    var hitPoints: Int
    var name: String
    init(playerName: String, startingHealth: Int) {
        self.name = playerName
        self.hitPoints = startingHealth
    }
    mutating func takeDamage(damage: Int) {
            hitPoints -= damage
    }
    func SuperAttack() -> Int {
        return 150
    }
    mutating func healHealth() {
        hitPoints += 15
    }
}

class MainPlayer : PlayerProtocol , MagicProtocol {
    func fireAttack() -> Int {
        return 5
    }
    func iceAttack() -> Int {
        return 10
    }
    func waterAttack() -> Int {
        return 20
    }
    var hitPoints: Int
    var name: String
    required init(playerName: String, startingHealth: Int) {
        self.name = playerName
        self.hitPoints = startingHealth
    }
    func takeDamage(damage: Int) {
        hitPoints -= damage
    }
}

class FinalBoss : PlayerProtocol , MagicProtocol , BossProtocol {
    var hitPoints: Int
    var name: String
    required init(playerName: String, startingHealth: Int) {
        self.name = playerName
        self.hitPoints = startingHealth
    }
    func takeDamage(damage: Int) {
        hitPoints -= damage
    }
    func fireAttack() -> Int {
        return 50
    }
    func iceAttack() -> Int {
        return 70
    }
    func waterAttack() -> Int {
        return 100
    }
    func SuperAttack() -> Int {
        return 200
    }
    func healHealth() {
        hitPoints += 25
    }
}

var miniBoss = MiniBoss(playerName: "karaken", startingHealth: 50)
miniBoss.tauntPlayer()
miniBoss.name
miniBoss.hitPoints
miniBoss.SuperAttack()

var finalBoss = FinalBoss(playerName: "Tekken", startingHealth: 100)
finalBoss.tauntPlayer()
finalBoss.name
finalBoss.fireAttack()
finalBoss.iceAttack()
finalBoss.waterAttack()
finalBoss.SuperAttack()

var mainPlayer = MainPlayer(playerName: "Hamza", startingHealth: 1000)

while miniBoss.isAlive(){
    miniBoss.takeDamage(damage: mainPlayer.waterAttack())
    mainPlayer.takeDamage(damage: miniBoss.SuperAttack())
}

while finalBoss.isAlive(){
    finalBoss.takeDamage(damage: mainPlayer.iceAttack())
    mainPlayer.takeDamage(damage: finalBoss.fireAttack())
}

if mainPlayer.isAlive() {
    print("Main player hit points are : \(mainPlayer.hitPoints)")
}
