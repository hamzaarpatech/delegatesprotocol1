//
//  SendingVC.swift
//  DelegatesProtocol1
//
//  Created by Hamza on 2/6/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit


protocol DataSentDelegate{
    func UserDidEnterData(data : String)
}

class SendingVC: UIViewController {
  
    @IBOutlet weak var sendingTextField: UITextField!
    var delegate : DataSentDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        if delegate != nil {
                let data =  sendingTextField.text
                delegate?.UserDidEnterData(data: data!)
                dismiss(animated: true, completion: nil)
        }
    }
}
