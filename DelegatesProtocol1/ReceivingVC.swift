//
//  ReceivingVC.swift
//  DelegatesProtocol1
//
//  Created by Hamza on 2/6/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ReceivingVC : UIViewController , DataSentDelegate {
   
    @IBOutlet weak var receivingLabelText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    func UserDidEnterData(data: String) {
        receivingLabelText.text = data
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendID"  {
            let sendingVC = segue.destination as! SendingVC
            sendingVC.delegate = self
        }
    }
}
